extends Node2D

enum STATE {
	IDLE,
	ANGRY,
	ALERT,
	DEAD
}

var _can_move = true
var _state = STATE.IDLE

func _ready():
	$moveRayCast2D.add_exception($Area2D)
	$moveRayCast2D.add_exception($HitArea)
	randomize()
	$Dead.visible = false

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass


func can_react_to_light():
	return _state == STATE.IDLE

func react_to_light():
	print("React to light")
	get_tree().get_nodes_in_group("trauma_listeners")[0].add_trauma(0.5)
	set_state(STATE.ANGRY)
	eye_color_tween(Color(red))
	
	$AnimationPlayer.playback_speed = 1
	$AnimationPlayer.play("react_to_light")
	$sfx/discovered.play()
	$turnTimer.start()

func chase_player():
	var player = get_tree().get_nodes_in_group("players")[0]
	chase_thing(player)


func chase_thing(target_node:Node):
	if $moveTween.is_active():
		yield($moveTween,"tween_completed")
	
	print("Chasing " + target_node.name)
		
	var target_dir : Vector2 = target_node.position- position
	var thing
	var new_dir = target_dir
	if target_dir.x != 0 and target_dir.y != 0:
		if abs(target_dir.x) < 100:
			target_dir.x = 0
		elif abs(target_dir.y) < 100:
			target_dir.y = 0
		else:
			print("Dir is too diagonal!!!")
			if(randf()> 0.5):
				new_dir.x = 0
				if not can_move_to(new_dir):
					new_dir.x = target_dir.x
					new_dir.y = 0
		#			yield(get_tree().create_timer(0.01), "timeout")
					if not can_move_to(new_dir):
						$turnTimer.start()
						print("No easy target.")
						return
			else:
				new_dir.y = 0
				if not can_move_to(new_dir):
					new_dir.y = target_dir.y
					new_dir.x = 0
		#			yield(get_tree().create_timer(0.01), "timeout")
					if not can_move_to(new_dir):
						$turnTimer.start()
						print("No easy target.")
						return
			new_dir = new_dir.normalized()
	
	if not can_move_to(new_dir):
		$turnTimer.start()
		return
	move(new_dir)


func can_move_to(dir):
	$moveRayCast2D.cast_to = dir.normalized() * global.tile_size * 0.75
	print("Checking dir " + String($moveRayCast2D.cast_to))
	$moveRayCast2D.force_update_transform()
	$moveRayCast2D.force_raycast_update()
	var thing = $moveRayCast2D.get_collider()
	if thing:
		return thing.get_parent().has_method("move") or (thing.get_parent().has_method("harm_monster") and thing.get_parent().harm_monster())
	else:
		return true
	

func move(dir):
	if not _can_move:
		return
#	$AnimationPlayer.play("Jump")
	$moveTween.interpolate_property(self,"position", position, position + (global.tile_size * dir.normalized()
	), $turnTimer.wait_time, Tween.TRANS_SINE, Tween.EASE_OUT)
	$AnimationPlayer.playback_speed = 0.5
	$AnimationPlayer.stop()
	$AnimationPlayer.play("Monster_walk")
	$moveTween.start()
	$turnTimer.start()

var red = "ff007e"

func light_toggled():
		
	if _state == STATE.ANGRY:
		_state = STATE.ALERT
		var diversions = get_tree().get_nodes_in_group("diversion_for_monster")
		if diversions.size()>0:
			if $moveTween.is_active():
				yield($moveTween,"tween_completed")
			yield(get_tree().create_timer(0.25, true), "timeout")
			chase_thing(diversions[randi() % diversions.size()])

		else:
			eye_color_tween(Color("ffe300"))
		
	elif _state == STATE.ALERT:
		set_state(STATE.ANGRY)
		eye_color_tween(Color(red))
		if $moveTween.is_active():
			yield($moveTween,"tween_completed")
		yield(get_tree().create_timer(0.25, true), "timeout")
		chase_player()

func set_state(state):
	_state = state
	if state == STATE.ANGRY:
		var notifys = get_tree().get_nodes_in_group("notify_monster_angry")
		for notify in notifys:
			notify.notify_monster_angry()

func harm_player():
	return _state == STATE.ANGRY or _state == STATE.ALERT

func _on_turnTimer_timeout():
	if _state == STATE.ANGRY:
		chase_player()
	elif _state == STATE.ALERT:
		var diversions = get_tree().get_nodes_in_group("diversion_for_monster")
#		yield(get_tree().create_timer(0.25, true), "timeout")
		for thing in diversions:
			if thing:
				print("Chasing diversion")
				chase_thing(thing)
				return

func eye_color_tween(color):
	$Monster/enemy_body/enemy_head/enemy_eyes/Tween.interpolate_property(
	$Monster/enemy_body/enemy_head/enemy_eyes, "modulate", $Monster/enemy_body/enemy_head/enemy_eyes.modulate, color, 0.25, Tween.TRANS_SINE, Tween.EASE_IN)
	$Monster/enemy_body/enemy_head/enemy_eyes/Tween.start()

func react_to_hurting_player(player):
#	$moveTween.stop_all()
	_can_move = false
	player.get_node("BloodSplatter").emitting = true


func _on_HitArea_area_entered(area):
	if area.get_parent().has_method("harm_monster") and area.get_parent().harm_monster():
		get_hurt(area.get_parent())
	
	
func get_hurt(thing):
	_state = STATE.DEAD
	_can_move = false
	if $moveTween.is_active():
			yield($moveTween,"tween_completed")
			
	if thing.has_method("react_to_hurting_monster"):
		thing.react_to_hurting_monster()
	$sfx/death.play()
	eye_color_tween(Color("00ff0000"))
	$Monster.visible = false
	$Dead.visible = true
#	if $moveTween.is_active():
#		yield($moveTween,"tween_completed")
